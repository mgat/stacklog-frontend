import { combineReducers } from 'redux'
import authedUser from './authedUser'
import users from './users'
import todos from './todos'
import columns from './columns'
import { loadingBarReducer } from 'react-redux-loading'

export default combineReducers({
	authedUser,
	users,
	todos,
	columns,
	loadingBar: loadingBarReducer,
})