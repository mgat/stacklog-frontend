import { RECEIVE_COLUMNS, UPDATE_COLUMNS, REORDER_COLUMN, UPDATE_BACKLOG, CHANGE_COLUMNS } from '../actions/columns'

export default function columns (state = {}, action){
	switch(action.type){
		case RECEIVE_COLUMNS :
			return {
				...state,
				...action.columns
			}
		case UPDATE_COLUMNS :
			return {
				...state,
				...action.columns
			}
		case CHANGE_COLUMNS:
			return {
				...state,
				...action.columns
			}
		case UPDATE_BACKLOG :
			return {
				...state,
				['backlog']: action.backlog
			}
		case REORDER_COLUMN: 
			return {
				...state,
				[action.column.id]: action.column
			}
		default :
			return state
	}
}