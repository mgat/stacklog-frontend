import { RECEIVE_TODOS, TOGGLE_TODO, ADD_TODO, REORDER_TODOS, SCHEDULE_TODO } from '../actions/todos'

export default function todos (state = {}, action){
	switch(action.type){
		case RECEIVE_TODOS :
			return {
				...state,
				...action.todos,
			}
		case TOGGLE_TODO :
			return {
				...state,
				[action.id]:{
					...state[action.id],
					completed: action.hasCompleted === true
						? state[action.id].completed.filter((uid) => uid!== action.authedUser)
						: state[action.id].completed.concat([action.authedUser])
				}
			}
		case SCHEDULE_TODO:
			return {
				...state,
				[action.id]:{
					...state[action.id],
					scheduled: action.day
				}
			}
		case ADD_TODO:
			return {
				...state,
				[action.todo.id]: action.todo,
			}
		case REORDER_TODOS:
			const {todos} = action
			return {
				todos
			}
		default :
			return state
	}
}