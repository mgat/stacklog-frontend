import React, { Component } from 'react'
import { connect } from 'react-redux'
import Week from './Week'

class Calendar extends Component {
	render() {
		//console.log(this.props)
		return(
			<div className='calendar'>
				<h3 className='center'>Calendar</h3>
				<Week />
			</div>
		)
	}
}

function mapStateToProps ({ todos }) {
	return {
		todoIds: Object.keys(todos)
			.sort((a,b) => todos[b].timestamp - todos[a].timestamp)
	}

}

export default connect(mapStateToProps)(Calendar)