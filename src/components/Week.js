import React, { Component } from 'react'
import { connect } from 'react-redux'
import Day from './Day'

class Week extends Component {
	render() {
		//console.log(this.props)
		return(
			<div className='week'>
				<Day day='2020-7-26'/>
				<Day day='2020-7-27'/>
				<Day day='2020-7-28'/>
				<Day day='2020-7-29'/>
				<Day day='2020-7-30'/>
				<Day day='2020-7-31'/>
			</div>
		)
	}
}

function mapStateToProps ({ todos, columns }) {
	return {
		todoIds: Object.keys(todos)
			.sort((a,b) => todos[b].timestamp - todos[a].timestamp),
		columns: columns
	}

}

export default connect(mapStateToProps)(Week)