import React, { Component } from 'react'
import { connect } from 'react-redux'
import Day from './Day'

class Schedule extends Component {
	render() {
		//console.log(this.props)
		return(
			<div className='schedule'>
				<h3 className='center'>Schedule</h3>
				<Day day='2020-7-25'/>
			</div>
		)
	}
}

function mapStateToProps ({ todos }) {
	return {
		todoIds: Object.keys(todos)
			.sort((a,b) => todos[b].timestamp - todos[a].timestamp)
	}

}

export default connect(mapStateToProps)(Schedule)