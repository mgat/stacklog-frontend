import React, { Component } from 'react'
import { connect } from 'react-redux'
import { formatTodo, formatDate} from '../utils/helpers'
import { TiTickOutline } from 'react-icons/ti'
import { TiTick } from 'react-icons/ti' 
import { handleToggleTodo } from '../actions/todos'
import { Link, withRouter } from 'react-router-dom'
import { Draggable } from 'react-beautiful-dnd'
import styled from 'styled-components'

const Container = styled.div`
	border: 1px solid lightgrey;
	border-radius:2px;
	padding: 8px;
	margin-bottom: 8px;
	background-color: ${props => (props.isDragging ? 'lightgreen' : 'white')};
`;

const TodoHeader = styled.div`
	background-color: white;
	width: 100%;
`


class Todo extends Component {
	handleComplete = (e) => {
		e.preventDefault()
		
		const {dispatch, todo, authedUser} = this.props

		dispatch(handleToggleTodo({
			id: todo.id,
			hasCompleted: todo.hasCompleted,
			authedUser: authedUser
		}))

	}


	showEditDialog = (e,id) => {
		e.preventDefault()
		this.props.history.push(`/todo/${id}`)


	}

	


	render (){
		//console.log(this.props)
		const { todo } = this.props

		if (todo === null){
			return <p>This Todo does not exist</p>
		}


		const {
			timestamp, text, hasCompleted, id, title
		} = todo

		return (
			<Draggable draggableId={id} index={this.props.index}>
				{(provided, snapshot) => (
					<Container
						{...provided.draggableProps}
						{...provided.dragHandleProps}
						ref={provided.innerRef}
						isDragging={snapshot.isDragging}
					>
						<TodoHeader>
								<p className="left">{title}</p>
								<Link className="right" to={`/todo/${id}`}>
									Edit
								</Link>
						</TodoHeader>
								<div>Due {formatDate(timestamp)}</div>
								
								<p
									style={{textDecoration: hasCompleted ? 'line-through' : 'none'}}
								>
								{text}
								</p>
								<button onClick={this.handleComplete}>
									{hasCompleted === true
										? <TiTick color='#34e024' />
										: <TiTickOutline />}
								</button>
						
					</Container>
				)}
			</Draggable>
		)
	}
}

//what state does this component need from the redux store
//mapStateToProps(state, [ownProps])
function mapStateToProps ({authedUser, users, todos}, { id }) {
	const todo = todos[id]

	return {
		authedUser,
		todo: todo 
			? formatTodo(todo,users[todo.author],authedUser)
			: null
	}
}
//withRouter passes the Router props like history
export default withRouter(connect(mapStateToProps)(Todo))