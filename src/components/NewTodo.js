import React, { Component } from 'react'
import { connect } from 'react-redux'
import { handleAddTodo } from '../actions/todos'
import { handleUpdateColumns } from '../actions/columns'
import { Redirect } from 'react-router-dom'

// controlled component
// whenever updating UI based on the current satet of component -> controlled component
class NewTodo extends Component {
	state = {
		text: '',
		toHome: false,
	}

	handleChange = (e) => {
		const text = e.target.value

		// use react component state over redux state because their is no benefit of putting
		// this form sate into inside of redux and it would be more complicated
		this.setState(()=> ({
			text
		}))
	}

	handleSubmit = (e) => {
		e.preventDefault()

		const { text } = this.state
		const { dispatch, id } = this.props

		dispatch(handleAddTodo(text, id))
		.then(() => dispatch(handleUpdateColumns()))
		


		this.setState(() => ({
			text: '',
			toHome: id ? false : true,
		}))
	}

	render() {
		const { text, toHome } = this.state

		if (toHome === true) {
			return <Redirect to='/' />
		}

		const todoLeft = 250 - text.length

		return (
			<div>
				<h3 className='center'>Compose new Todo</h3>
				<form className='new-todo' onSubmit={this.handleSubmit}>
					<textarea
						placeholder="Type in your todo"
						value={text}
						onChange={this.handleChange}
						className='textarea'
						maxLength={250}
					/>
					{todoLeft <= 100 && (
						<div className='todo-length'>
							{todoLeft}
						</div>
					)}
					<button
						className='btn'
						type='submit'
						disabled={text === ''}
					>
						Submit
					</button>
				</form>
			</div>
		)
	}
}

function mapStateToProps ({ todos }) {
	return {
		todos : todos

	}
}

export default connect(mapStateToProps)(NewTodo)