import React, { Component, Fragment } from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { connect } from 'react-redux'
import { handleInitialData } from '../actions/shared'
import Home from './Home'
import LoadingBar from 'react-redux-loading'
import NewTodo from './NewTodo'
import TodoPage from './TodoPage'
import Nav from './Nav'


class App extends Component {

	componentDidMount(){
		this.props.dispatch(handleInitialData())
	}


	render() {
    	return (
        <Router>
          <Fragment>
            <LoadingBar/>
            <div className='container'>
              <Nav />
              {this.props.loading === true
                ? null
                : <div>
                    <Route path='/' exact component={Home} />
                    <Route path='/todo/:id' component={TodoPage} />
                    <Route path='/new'  component={NewTodo} />
                  </div> 
              }
            </div>
          </Fragment>
        </Router>
      	
    	)
  	}
}

function mapStateToProps ({authedUser}) {
	return {
		loading: authedUser === null
	}
}

export default connect(mapStateToProps)(App)