import React, { Component } from 'react'
import { connect } from 'react-redux'
import Backlog from './Backlog'
import Schedule from './Schedule'
import Calendar from './Calendar'
import {DragDropContext} from "react-beautiful-dnd";
import styled from "styled-components";
import { handleReorderColumn, handleChangeColumns } from '../actions/columns'
import { handleScheduleTodo } from '../actions/todos'





class Home extends Component {

	handleReorder = (newColumn) => {
		
		const {dispatch} = this.props

		dispatch(handleReorderColumn(newColumn))
		
	}

	handleColumnChange = (newColumns,draggableId,day) => {
		
		const {dispatch} = this.props

		dispatch(handleChangeColumns(newColumns))
		dispatch(handleScheduleTodo(draggableId,day))
		
	}



	onDragEnd = result => {
		const { destination, source, draggableId } = result;

		console.log("on DRAG Dest", destination)
		console.log("on DRAG Dest", source)
		console.log("on DRAG Dest", draggableId)

		if (!destination) {
			return;
		}

		if (
			destination.droppableId === source.droppableId &&
			destination.index === source.index
		) {
			return;
		}

		const start = this.props.columns[source.droppableId];
		const finish = this.props.columns[destination.droppableId];

		// Moving in same column
		if (start === finish) {
			const newTodoIds = Array.from(start.todoIds);
			//splice(index,#items,insert):
			//from source.index remove 1 item
			newTodoIds.splice(source.index,1);
			//remove nothing and insert draggableId
			newTodoIds.splice(destination.index,0,draggableId)

			const newColumn = {
				...start,
				todoIds: newTodoIds,

			}
			console.log("New Column",newColumn)

			this.handleReorder(newColumn)
			return;
		}

		// Moving from one column to another
		const startTodoIds = Array.from(start.todoIds);
		startTodoIds.splice(source.index,1);
		const newStart = {
				...start,
				todoIds: startTodoIds,
		}

		console.log("FINISH",finish)
		const finishTodoIds = Array.from(finish.todoIds);
		finishTodoIds.splice(destination.index,0,draggableId)
		const newFinish = {
				...finish,
				todoIds: finishTodoIds,
		}

		const newColumns = {
			[newStart.id] : newStart,
			[newFinish.id]: newFinish
		}

		this.handleColumnChange(newColumns,draggableId,newFinish.id)

		
	};

	render() {
		//console.log(this.props)
		return(
			<DragDropContext onDragEnd={this.onDragEnd}>
				<div className='home'>
						<Backlog />
						<Schedule/>
						<Calendar/>
				</div>
			</DragDropContext>
			
		)
	}

	
}

function mapStateToProps ({ todos, columns}) {
	return {
		todoIds: Object.keys(todos)
			.sort((a,b) => todos[b].timestamp - todos[a].timestamp),
		columns: columns,
	}

}


export default connect(mapStateToProps)(Home)