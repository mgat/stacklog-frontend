import React, { Component } from 'react'
import { connect } from 'react-redux'
import Todo from './Todo'
import { TiPlus } from 'react-icons/ti'
import { Link} from 'react-router-dom'
import { Droppable } from "react-beautiful-dnd";
import styled from 'styled-components'

const TaskList = styled.div`
	padding: 8px;
	transition: background-color 0.2s ease;
	background-color: ${props => (props.isDraggingOver ? 'skyblue' : 'white')};
`

class Backlog extends Component {
	render() {

		const column = this.props.columns['backlog']
		//console.log(this.props)
		return(
			<div className='backlog'>
				<h3 className='center'>Backlog<Link to='/new' >
					<TiPlus  className='todo-icon' />
				</Link></h3>
				
				<Droppable droppableId='backlog'>
					{(provided, snapshot) => (
						<TaskList
							ref={provided.innerRef}
							{...provided.droppableProbs}
							isDraggingOver={snapshot.isDraggingOver}
						>
							{column.todoIds.map((id, index) => (
								<Todo key ={id} id={id} index={index}/>
							))}
							{provided.placeholder}
						</TaskList>
					)}
				</Droppable>
				
			</div>
		)
	}
}

function mapStateToProps ({ todos , columns}) {
	return {
		todoIds: Object.keys(todos)
			.filter((a) => todos[a].scheduled === '')
			.sort((a,b) => todos[b].timestamp - todos[a].timestamp),
		columns: columns,

	}
}

export default connect(mapStateToProps)(Backlog)