import React, { Component } from 'react'
import { connect } from 'react-redux'
import Todo from './Todo'
import { Droppable } from "react-beautiful-dnd";
import styled from 'styled-components'

const TaskList = styled.div`
	padding: 8px;
	transition: background-color 0.2s ease;
	background-color: ${props => (props.isDraggingOver ? 'skyblue' : 'white')};
`

class Day extends Component {
	render() {

		const column = this.props.columns[this.props.day]
		//console.log(this.props)
		return(
			<div className='day'>
				<Droppable droppableId={this.props.day}>
					{(provided, snapshot) => (
						<TaskList
							ref={provided.innerRef}
							{...provided.droppableProbs}
							isDraggingOver={snapshot.isDraggingOver}
						>
							{column.todoIds.map((id, index) => (
								<Todo key ={id} id={id} index={index}/>
							))}
							{provided.placeholder}
						</TaskList>
					)}
				</Droppable>
				
			</div>
		)
	}
}

function mapStateToProps ({ todos , columns}, {day}) {
	return {
		todoIds: Object.keys(todos)
			.filter((a) => todos[a].scheduled === day)
			.sort((a,b) => todos[b].timestamp - todos[a].timestamp),
		columns: columns,

	}
}

export default connect(mapStateToProps)(Day)