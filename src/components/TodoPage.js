import React, { Component } from 'react'
import { connect } from 'react-redux'
import Todo from './Todo'
import NewTodo from './NewTodo'

class TodoPage extends Component {
	render() {
		//console.log(this.props)
		const { id } = this.props
		return(
			<div>
				<Todo id = {id}/>
				<NewTodo id={id}/>
			</div>
			)
	}
}

function mapStateToProps({ authedUser, todos, users}, props){
	const { id } = props.match.params

	return {
		id,

	}
}

export default connect(mapStateToProps)(TodoPage)