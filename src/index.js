import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './components/App'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
// import the default export coming from the index.js file in the reducer folder -> combineReducers
import reducer from './reducers'
// import the default export coming from the index.js file in the middleware folder -> applyMiddleware
import middleware from './middleware'

const store = createStore(reducer, middleware)



ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById('root')
)