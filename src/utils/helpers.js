export function formatDate (timestamp) {
  const d = new Date(timestamp)
  const time = d.toLocaleTimeString('en-US')
  //return time.substr(0, 5) + time.slice(-2) + ' | ' + d.toLocaleDateString()
  return d.toLocaleDateString()
}

export function formatTodo (todo, author, authedUser) {
  const { id, completed, text, timestamp , title} = todo
  const { name, avatarURL } = author

  return {
    name,
    id,
    timestamp,
    text,
    title,
    avatar: avatarURL,
    completed: completed.length,
    hasCompleted: completed.includes(authedUser),
  }
}