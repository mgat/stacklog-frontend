import {
  _getUsers,
  _getTodos,
  _saveCompleteToggle,
  _saveTodo,
  _getColumns,
  _saveDay,
} from './_DATA.js'

export function getInitialData () {
  return Promise.all([
    _getUsers(),
    _getTodos(),
    _getColumns(),
  ]).then(([users, todos, columns]) => ({
    users,
    todos,
    columns,
  }))
}

export function saveCompleteToggle (info) {
  return _saveCompleteToggle(info)
}

export function saveTodo (info) {
  return _saveTodo(info)
}

export function saveDay (info) {
  return _saveTodo(info)
}