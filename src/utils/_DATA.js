let columns = {
  'backlog': {
    id: 'backlog',
    title: 'Backlog',
    section: 'backlog',
    todoIds: ['8xf0y6ziyjabvozdd253nd', '5c9qojr2d1738zlx09afby','f4xzgapq7mu783k9t02ghx','hbsc73kzqi75rg7v1e0i6a'],
  },
  'schedule': {
      id: 'column-2',
      title: '00:00',
      section: 'dailySchedule',
      todoIds: [],
  },
  'day1': {
      id: 'column-3',
      title: '01:00',
      section: 'dailySchedule',
      todoIds: [],
  },
  'column-4': {
      id: 'column-4',
      title: '02:00',
      section: 'dailySchedule',
      todoIds: [],
  },
  'column-5': {
      id: 'column-5',
      title: '03:00',
      section: 'dailySchedule',
      todoIds: [],
  },
  'column-6': {
      id: 'column-6',
      title: '04:00',
      section: 'dailySchedule',
      todoIds: [],
  },
  'column-7': {
      id: 'column-7',
      title: '05:00',
      section: 'dailySchedule',
      todoIds: [],
  }
}

let users = {
  anna: {
    id: "anna",
    name: "Anna",
    avatarURL: "https://tylermcginnis.com/would-you-rather/sarah.jpg",
    todos: ['8xf0y6ziyjabvozdd253nd', 'hbsc73kzqi75rg7v1e0i6a', '2mb6re13q842wu8n106bhk', '6h5ims9iks66d4m7kqizmv', '3sklxkf9yyfowrf0o1ftbb'],
  },
  alex: {
    id: "alex",
    name: "Alex",
    avatarURL: "https://tylermcginnis.com/would-you-rather/tyler.jpg",
    todos: ['5c9qojr2d1738zlx09afby', 'f4xzgapq7mu783k9t02ghx', 'nnvkjqoevs8t02lzcc0ky', '4pt0px8l0l9g6y69ylivti', 'fap8sdxppna8oabnxljzcv', 'leqp4lzfox7cqvsgdj0e7', '26p5pskqi88i58qmza2gid', 'xi3ca2jcfvpa0i3t4m7ag'],
  },
  thomas: {
    id: "thomas",
    name: "Thomas",
    avatarURL: "https://tylermcginnis.com/would-you-rather/dan.jpg",
    todos: ['5w6k1n34dkp1x29cuzn2zn', 'czpa59mg577x1oo45cup0d', 'omdbjl68fxact38hk7ypy6', '3km0v4hf1ps92ajf4z2ytg', 'njv20mq7jsxa6bgsqc97', 'sfljgka8pfddbcer8nuxv', 'r0xu2v1qrxa6ygtvf2rkjw'],
  },
  marcel: {
    id: "marcel",
    name: "Marcel",
    avatarURL: "https://tylermcginnis.com/would-you-rather/dan.jpg",
    todos: ['5w6k1n34dkp1x29cuzn2zn', 'czpa59mg577x1oo45cup0d', 'omdbjl68fxact38hk7ypy6', '3km0v4hf1ps92ajf4z2ytg', 'njv20mq7jsxa6bgsqc97', 'sfljgka8pfddbcer8nuxv', 'r0xu2v1qrxa6ygtvf2rkjw'],
  }

}

let todos = {
  "8xf0y6ziyjabvozdd253nd": {
    id: "8xf0y6ziyjabvozdd253nd",
    title:"TODO",
    text: "Run a marathon!",
    author: "anna",
    timestamp: 1518122597860,
    duedate: 1518122597860,
    completed: ['marcel'],
    scheduled: '2020-6-26',
  },
  "5c9qojr2d1738zlx09afby": {
    id: "5c9qojr2d1738zlx09afby",
    title:"TODO",
    text: "Walk the dog",
    author: "marcel",
    timestamp: 1518043995650,
    duedate: 1518043995650,
    completed: ['anna', 'alex'],
    scheduled: '2020-6-27',
  },
  "f4xzgapq7mu783k9t02ghx": {
    id: "f4xzgapq7mu783k9t02ghx",
    title:"TODO",
    text: "Learn React",
    author: "marcel",
    timestamp: 1517043995650,
    duedate: 1517043995650,
    completed: ['alex'],
    scheduled: '2020-6-26',
  },
  "hbsc73kzqi75rg7v1e0i6a": {
    id: "hbsc73kzqi75rg7v1e0i6a",
    title:"TODO",
    text: "Go shopping",
    author: "anna",
    timestamp: 1516043995650,
    duedate: 1516043995650,
    completed: ['marcel'],
    scheduled: '2020-6-26',
  },
  "5w6k1n34dkp1x29cuzn2zn": {
    id: "5w6k1n34dkp1x29cuzn2zn",
    title:"TODO",
    text: "Make dinner",
    author: "alex",
    timestamp: 1515043995650,
    duedate: 1515043995650,
    completed: ['anna'],
    scheduled: '2020-6-28',
  },
  "czpa59mg577x1oo45cup0d": {
    id: "czpa59mg577x1oo45cup0d",
    title:"TODO",
    text: "Meet with friends",
    author: "alex",
    timestamp: 1515043995650,
    duedate: 1515043995650,
    completed: ['marcel'],
    scheduled: 'backlog',
  },
  "2mb6re13q842wu8n106bhk": {
    id: "2mb6re13q842wu8n106bhk",
    title:"TODO",
    text: "Learn math",
    author: "anna",
    timestamp: 1514043995650,
    duedate: 1514043995650,
    completed: ['alex'],
    scheduled: '2020-6-29',
  },
  "nnvkjqoevs8t02lzcc0ky": {
    id: "nnvkjqoevs8t02lzcc0ky",
    title:"TODO",
    text: "Read a book",
    author: "marcel",
    timestamp: 1513043995650,
    duedate: 1513043995650,
    completed: [],
    scheduled: '2020-6-22',
  },
  "omdbjl68fxact38hk7ypy6": {
    id: "omdbjl68fxact38hk7ypy6",
    title:"TODO",
    text: "Read a second book",
    author: "alex",
    timestamp: 1512043995650,
    duedate: 1512043995650,
    completed: [],
    scheduled: '2020-6-23',
  },
  "4pt0px8l0l9g6y69ylivti": {
    id: "4pt0px8l0l9g6y69ylivti",
    title:"TODO",
    text: "Talk to James",
    author: "marcel",
    timestamp: 1511043995650,
    duedate: 1511043995650,
    completed: ['alex'],
    scheduled: '2020-6-24',
  },
  "6h5ims9iks66d4m7kqizmv": {
    id: "6h5ims9iks66d4m7kqizmv",
    title:"TODO",
    text: "Clean up",
    author: "anna",
    timestamp: 1510043995650,
    duedate: 1510043995650,
    completed: ['marcel'],
    title:"TODO",
    scheduled: '2020-6-25',
  },
  "fap8sdxppna8oabnxljzcv": {
    id: "fap8sdxppna8oabnxljzcv",
    title:"TODO",
    author: "marcel",
    text: "Watch the video",
    timestamp: 1518122677860,
    duedate: 1518122677860,
    completed: ['anna'],
    scheduled: 'backlog',
  },
  "3km0v4hf1ps92ajf4z2ytg": {
    id: "3km0v4hf1ps92ajf4z2ytg",
    title:"TODO",
    author: "alex",
    text: "Plan the month",
    timestamp: 1518122667860,
    duedate: 1518122667860,
    completed: [],
    scheduled: 'backlog',
  },
  "njv20mq7jsxa6bgsqc97": {
    id: "njv20mq7jsxa6bgsqc97",
    title:"TODO",
    author: "alex",
    text: "Ride the bike",
    timestamp: 1518044095650,
    duedate: 1518044095650,
    completed: ['marcel'],
    scheduled: 'backlog',
  },
  "leqp4lzfox7cqvsgdj0e7": {
    id: "leqp4lzfox7cqvsgdj0e7",
    title:"TODO",
    author: "marcel",
    text: "Hello",
    timestamp: 1516043255650,
    duedate: 1516043255650,
    completed: [],
    scheduled: 'backlog',
  },
  "sfljgka8pfddbcer8nuxv": {
    id: "sfljgka8pfddbcer8nuxv",
    title:"TODO",
    author: "alex",
    text: "world",
    timestamp: 1516045995650,
    duedate: 1516045995650,
    completed: ['anna', 'marcel'],
    scheduled: 'backlog',
  },
  "3sklxkf9yyfowrf0o1ftbb": {
    id: "3sklxkf9yyfowrf0o1ftbb",
    title:"TODO",
    author: "anna",
    text: "Practise the concept",
    timestamp: 1515044095650,
    duedate: 1515044095650,
    completed: ['alex'],
    scheduled: 'backlog',
  },
  "26p5pskqi88i58qmza2gid": {
    id: "26p5pskqi88i58qmza2gid",
    title:"TODO",
    author: "marcel",
    text: "Call mum",
    timestamp: 1514044994650,
    duedate: 1514044994650,
    completed: ['anna'],
    scheduled: 'backlog',
  },
  "xi3ca2jcfvpa0i3t4m7ag": {
    id: "xi3ca2jcfvpa0i3t4m7ag",
    title:"TODO",
    author: "marcel",
    text: "Make Research",
    timestamp: 1510043995650,
    duedate: 1510043995650,
    completed: [],
    scheduled: 'backlog',
  },
  "r0xu2v1qrxa6ygtvf2rkjw": {
    id: "r0xu2v1qrxa6ygtvf2rkjw",
    title:"TODO",
    author: "alex",
    text: "Count the money",
    timestamp: 1510044395650,
    duedate: 1510044395650,
    completed: ['marcel'],
    scheduled: 'backlog',
  },
}

export function _getUsers () {
  return new Promise((res, rej) => {
    setTimeout(() => res({...users}), 1000)
  })
}

export function _getTodos () {
  return new Promise((res, rej) => {
    setTimeout(() => res({...todos}), 1000)
  })
}

export function _getColumns () {
  return new Promise((res, rej) => {
    setTimeout(() => res({...columns}), 1000)
  })
}

export function _saveCompleteToggle ({ id, hasCompleted, authedUser }) {
  return new Promise((res, rej) => {
    setTimeout(() => {
      todos = {
        ...todos,
        [id]: {
          ...todos[id],
          completed: hasCompleted === true
            ? todos[id].completed.filter((uid) => uid !== authedUser)
            : todos[id].completed.concat([authedUser])
        }
      }

      res()
    }, 500)
  })
}

function generateUID () {
  return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)
}

function formatTweet ({ author, text, title}) {
  return {
    author,
    id: generateUID(),
    completed: [],
    text,
    title,
    timestamp: Date.now(),
    scheduled: 'backlog'
  }
}

export function _saveTodo ({ text, author}) {
  return new Promise((res, rej) => {
    const formattedTweet = formatTweet({
      text,
      author,
      title: text,
    })

    setTimeout(() => {
      todos = {
        ...todos,
        [formattedTweet.id]: formattedTweet,
      }

      users = {
        ...users,
        [author]: {
          ...users[author],
          todos: users[author].todos.concat([formattedTweet.id])
        }
      }

      res(formattedTweet)
    }, 1000)
  })
}


//TODO: just a placeholder copy from saceTodo
export function _saveDay ({ text, author}) {
  return new Promise((res, rej) => {
    const formattedTweet = formatTweet({
      text,
      author,
    })

    setTimeout(() => {
      todos = {
        ...todos,
        [formattedTweet.id]: formattedTweet,
      }

      users = {
        ...users,
        [author]: {
          ...users[author],
          todos: users[author].todos.concat([formattedTweet.id])
        }
      }

      res(formattedTweet)
    }, 1000)
  })
}

