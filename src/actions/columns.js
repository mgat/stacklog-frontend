export const RECEIVE_COLUMNS = 'RECEIVE_COLUMNS'
export const UPDATE_COLUMNS = 'UPDATE_COLUMNS'
export const REORDER_COLUMN = 'REORDER_COLUMN'
export const UPDATE_BACKLOG = 'UPDATE_BACKLOG'
export const CHANGE_COLUMNS = 'CHANGE_COLUMNS'

 function getDaysInMonth(month,year) {
  // Here January is 1 based
  //Day 0 is the last day in the previous month
 return new Date(year, month, 0).getDate();
// Here January is 0 based
// return new Date(year, month+1, 0).getDate();
};

export function receiveColumns (todos) {

	const now = new Date()
	const month = now.getMonth()+1
	const year = now.getYear()+1900
	const daysInMonth = getDaysInMonth(month,year)
//`${year}-${month}-${i+1}`
	
	//create array with keys for day columns
	let days = [...Array(daysInMonth).keys()].map( i => `${year}-${month}-${i+1}`)
	
	//create array of objects with year-month-day as key and id+todoIds as attributes
	let arr = []
	for (const d of days){
		arr.push({[d]:{id:d,todoIds:Object.keys(todos)
							.filter((a) => todos[a].scheduled === d)
							.sort((a,b) => todos[b].timestamp - todos[a].timestamp)}})
	}
	
	// create backlog object
	const backlog = {
				id: 'backlog',
				todoIds: Object.keys(todos)
							.filter((a) => todos[a].scheduled === 'backlog')
							.sort((a,b) => todos[b].timestamp - todos[a].timestamp),
			}

	
	// transform arr to columns object
	var columns = {};
		for (var i = 0; i < arr.length; i++) {
  			columns[Object.keys(arr[i])[0]]=Object.values(arr[i])[0]
		}
	columns['backlog'] = backlog

	
	// return columns object
	// uncomment for test column
	/*return {
		type: RECEIVE_COLUMNS,
		columns: {
			columns,
			backlog,
			'2020-6-26': {
				id: '2020-6-26',
				todoIds: Object.keys(todos)
							.filter((a) => todos[a].scheduled === '2020-6-26')
							.sort((a,b) => todos[b].timestamp - todos[a].timestamp),
			}
		}
	}*/
	return {
		type: RECEIVE_COLUMNS,
		columns,
	}
}

function updateColumns (todos) {
	return {
		type:UPDATE_COLUMNS,
		columns: {
			backlog: {
				id: 'backlog',
				todoIds: Object.keys(todos)
							.filter((a) => todos[a].scheduled === 'backlog')
							.sort((a,b) => todos[b].timestamp - todos[a].timestamp),
			},
		}
	}
}

function updateBacklog (todos) {
	return {
		type:UPDATE_BACKLOG,
		backlog: {
				id: 'backlog',
				todoIds: Object.keys(todos)
							.filter((a) => todos[a].scheduled === 'backlog')
							.sort((a,b) => todos[b].timestamp - todos[a].timestamp),
		},
	}
}


export function handleUpdateColumns () {
	return (dispatch, getState) => {
		const { todos } = getState()
		dispatch(updateColumns(todos))
	}
}

function reorderColumn(column){
	return{
		type: REORDER_COLUMN,
		column,
	}
}

export function handleReorderColumn (newColumn) {
	return (dispatch) => {
		dispatch(reorderColumn(newColumn))
	}
}

function changeColumns(columns){
	return{
		type: CHANGE_COLUMNS,
		columns
	}
}

export function handleChangeColumns (newColumns){
	return (dispatch) => {
		dispatch(changeColumns(newColumns))
	}
}