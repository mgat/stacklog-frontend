import { saveCompleteToggle, saveTodo } from '../utils/api'
import { showLoading, hideLoading } from 'react-redux-loading'

export const RECEIVE_TODOS = 'RECEIVE_TODOS'
export const TOGGLE_TODO = 'TOGGLE_TODO'
export const ADD_TODO = 'ADD_TODO'
export const REORDER_TODOS = 'REORDER_TODO'
export const SCHEDULE_TODO = 'SCHEDULE_TODO'


function addTodo (todo) {
	return {
		type:ADD_TODO,
		todo,
	}
}

export function handleAddTodo (text){
	return (dispatch, getState) => {
		const { authedUser } = getState()

		dispatch(showLoading())

		return saveTodo({
			text,
			author: authedUser,
		})
			.then((todo) => dispatch(addTodo(todo)))
			.then(() => dispatch(hideLoading()))


	}
}


function reorderTodos (todos) {
	return {
		type: REORDER_TODOS,
		todos
	}
}

export function handleReorderTodo (todos){
	return (dispatch) => {
		dispatch(reorderTodos(todos))
	}
}

export function receiveTodos (todos) {
	return {
		type: RECEIVE_TODOS,
		todos,
	}
}

function toggleTodo ({id, authedUser, hasCompleted}) {
	return {
		type: TOGGLE_TODO,
		id,
		authedUser,
		hasCompleted
	}
}

export function handleToggleTodo (info) {
	return (dispatch) => {
		dispatch(toggleTodo(info))

		return saveCompleteToggle(info)
			.catch((e) => {
				console.warn('Error in handleCompleteTodo: ',e)
					dispatch(toggleTodo(info))
					alert('Their was an error completing the todo. Try again')
			})
	}
}

function scheduleTodo(id,day){
	return{
		type: SCHEDULE_TODO,
		id,
		day,
	}
}

export function handleScheduleTodo(id,day) {
	return (dispatch) => {
		dispatch(scheduleTodo(id,day))
	}
}