import { getInitialData } from '../utils/api'
import { receiveUsers } from '../actions/users'
import { receiveTodos } from '../actions/todos'
import { receiveColumns } from '../actions/columns'
import { setAuthedUser } from '../actions/authedUser'
import {showLoading, hideLoading} from 'react-redux-loading'

const AUTHED_ID = 'marcel'

export function handleInitialData () {
	return (dispatch) => {
		dispatch(showLoading())
		return getInitialData()
			.then(({ users, todos}) => {
				dispatch(receiveUsers(users))
				dispatch(receiveTodos(todos))
				dispatch(receiveColumns(todos))
				dispatch(setAuthedUser(AUTHED_ID))
				dispatch(hideLoading())
			})
	}
}